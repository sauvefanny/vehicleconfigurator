package co.simplon.alt3.vehicleconfigurator.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="car")
public class Car extends Vehicle{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public Number horsePower;
    
    public Number getHorsePower() {
        return horsePower;
    }
    public void setHorsePower(Number horsePower) {
        this.horsePower = horsePower;
    }
    
}
