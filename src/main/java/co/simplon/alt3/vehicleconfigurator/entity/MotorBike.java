package co.simplon.alt3.vehicleconfigurator.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="motorbike")

public class MotorBike extends Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public Number cylinder;
    
    public Number getCylinder() {
        return cylinder;
    }
    public void setCylinder(Number cylinder) {
        this.cylinder = cylinder;
    }

    }
    
