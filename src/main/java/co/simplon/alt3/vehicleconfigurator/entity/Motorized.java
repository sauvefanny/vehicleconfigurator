package co.simplon.alt3.vehicleconfigurator.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="motorized")

public class Motorized extends Vehicle{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public Number kilometer;
    public LocalDate dateOfCirculation;

    public Number getKilometer() {
        return kilometer;
    }
    public void setKilometer(Number kilometer) {
        this.kilometer = kilometer;
    }
    public LocalDate getDateOfCirculation() {
        return dateOfCirculation;
    }
    public void setDateOfCirculation(LocalDate dateOfCirculation) {
        this.dateOfCirculation = dateOfCirculation;
    }

    
}
