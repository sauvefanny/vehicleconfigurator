package co.simplon.alt3.vehicleconfigurator.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unmotorized")

public class UnMotorized extends Vehicle{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    public Float numberOfPlates;
    
    public Float getNumberOfPlates() {
        return numberOfPlates;
    }
    public void setNumberOfPlates(Float numberOfPlates) {
        this.numberOfPlates = numberOfPlates;
    }


    
}